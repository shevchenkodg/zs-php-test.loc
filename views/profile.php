<h1>Ваша інформація</h1>
<?php
// Якщо інформація про користувача отримана
if(count($auth['data'])) {
	$data = $auth['data'];
	?>
		Ваш ID: <?php echo $data['id']; ?>;<br>
		Останній візит: <?php echo $data['last_visit']; ?>.<br><br>

		<a href="/authLogout.php">Вийти</a>
	<?php
}
?>