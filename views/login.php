<h1>Авторизація</h1>
<form id="loginForm">
	<input type="email" name="email" placeholder="E-mail" required>
	<input type="password" name="password" placeholder="Пароль" required>
	<br>
	<input type="checkbox" name="remember" value="1"> Залишатися в системі після закриття браузера
	<br>
	<input type="submit" value="Увійти">
</form>

<script src="/js/jquery.min.js"></script>

<script>
	$('#loginForm').submit(function(e){
		e.preventDefault();

		$.ajax({
	        url: '/authLogin.php',
	        type: 'post',
	        dataType: 'json',
	        data: $(this).serialize(),
	        success: function(data) {
	            if(data.status == true) {
	            	$(location).attr('href', '/');
	            }
	            if(data.message) {
            		alert(data.message)
            	}
	        },
	        error: function(xhr, str){
	        }
	    });
	});
</script>